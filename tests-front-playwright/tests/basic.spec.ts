// @ts-check
import {expect, test} from '@playwright/test';
import { GeneralPage } from '../pages/general-page';
import { Mocks } from '../helpers/mocks';
import { Intercept } from '../helpers/intercept';
import { fixtures } from '../data/fixtures';
import {allure} from "allure-playwright";


test.beforeEach(async ({ page }) => {
  await allure.feature("Basic tests");

  const intercept = new Intercept(page);
  await intercept.logNotMockedRequests();
});

test('Basic enter page test with empty table', async ({ page }) => {
  const generalPage = new GeneralPage(page);
  const mocks = new Mocks(page);
  const intercept = new Intercept(page);

  await mocks.getEmployeesList(200, '[]')

  intercept.waitForApiEmployersList();
  await generalPage.goToMainPage();

  await generalPage.checkMainSubtitleText()
  await generalPage.tableHeadIsVisible()

  await generalPage.tableHeadCellHaveText(1, 'Employee First Name')
  await generalPage.tableHeadCellHaveText(2, 'Employee Last Name')
  await generalPage.tableHeadCellHaveText(3, 'Employee Email Id')
  await generalPage.tableHeadCellHaveText(4, 'Actions')
});

test('Basic enter page test without mocks', async ({ page }) => {
  const generalPage = new GeneralPage(page);
  const intercept = new Intercept(page);

  intercept.waitForApiEmployersList();
  await generalPage.goToMainPage();
  await generalPage.checkMainSubtitleText()

});

test('Basic view employee check', async ({ page }) => {
  const generalPage = new GeneralPage(page);
  const mocks = new Mocks(page);

  const id = 1;
  const name = 'Ivan';
  const lastName = 'Pupkin';
  const email = 'pupkin_i@a.com';

  await mocks.getEmployeesList(200, `[{"id": ${id}, "firstName": "${name}", "lastName": "${lastName}", "emailId": "${email}"}]`)
  await mocks.getEmployer(1,200, `{"id": ${id}, "firstName": "${name}", "lastName": "${lastName}", "emailId": "${email}"}`)
  await generalPage.goToMainPage();

  await generalPage.viewButtonClick(1);
  await page.waitForURL(`/view-employee/${id}`);
});


test('Basic filled table rows check', async ({ page }) => {
  const generalPage = new GeneralPage(page);
  const mocks = new Mocks(page);

  const id = '1';
  const name = 'Ivan';
  const lastName = 'Pupkin';
  const email = 'pupkin_i@a.com';

  await mocks.getEmployeesList(200, `[{"id": ${id}, "firstName": "${name}", "lastName": "${lastName}", "emailId": "${email}"}]`)

  await generalPage.goToMainPage();

  await expect(page.locator('thead')).toBeVisible();

  await generalPage.tableHeadCellHaveText(1, 'Employee First Name')

  await generalPage.tableBodyAssertRowsCount(1)

  await generalPage.tableBodyCellHaveText(1, 1, name)
  await generalPage.tableBodyCellHaveText(1, 2, lastName)
  await generalPage.tableBodyCellHaveText(1, 3, email)

});


test('Check filled table', async ({ page }) => {
  const generalPage = new GeneralPage(page);
  const mocks = new Mocks(page);

  await mocks.getEmployeesList(200, fixtures.three_records);

  await page.goto('/employees');
  await generalPage.checkMainSubtitleText()

  await generalPage.tableBodyAssertRowsCount(3)

  await generalPage.tableBodyCellHaveText(2, 2, 'Vilkin')

});
