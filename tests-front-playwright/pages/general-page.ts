import {expect, Page} from '@playwright/test';
import {allure} from 'allure-playwright';

export class GeneralPage {

    readonly page: Page;

    constructor(page: Page) {
        this.page = page;
        allure.epic("[front] Tests for UI(playwright)").then(null);
    }

    // locators

    readonly nameInput = "[name='firstName']";
    readonly lastNameInput = "[name='lastName']";
    readonly emailInput = "[name='emailId']";
    readonly subTitleText = 'Employees List';

    // other

    public async checkMainSubtitleText() {
        await this.checkSubtitleText(this.subTitleText)

    }

    public async goToMainPage() {
        await allure.step("Go to main page ", async () => {
            await this.page.goto('/');
        });
    }

    public async fillEmployerForm(name: string, lastName: string, email: string,) {
        await allure.step(`Fill form with name: ${name}, lastName: ${lastName}, email: ${email}`, async () => {
            await this.formIsVisible();

            await this.fillInput(this.nameInput, name)
            await this.fillInput(this.lastNameInput, lastName)
            await this.fillInput(this.emailInput, email)
        });
    }

    // buttons

    public async addEmployerButtonClick() {
            await this.clickButtonByText('Add Employee');
    }

    public async saveButtonClick() {
            await this.clickButtonByText('Save');
    }

    public async cancelButtonClick() {
        await this.clickButtonByText('Cancel');
    }

    public async viewButtonClick(row: number) {
        await this.clickButtonByNameAndRow('View', row);
    }

    public async deleteButtonClick(row: number) {
        await this.clickButtonByDataQaAndRow('deleteButton', row);
    }

    // table

    public async tableHeadIsVisible() {
        await expect(this.page.locator('thead')).toBeVisible();
    }

    public async tableHeadCellHaveText(column: number, text: string) {
       await this.tableRow('thead', 'th', 1, column, text);
    }

    public async tableBodyCellHaveText(row: number, column: number, text: string) {
        await this.tableRow('tbody', 'td', row, column, text);
    }

    public async tableBodyAssertRowsCount(count: number) {
        await allure.step(`Check table body has ${count} rows`, async () => {
            await expect(this.page.locator('tbody').locator('tr')).toHaveCount(count);
        });

    }
    // private functions

    private async tableRow(part: string, tag: string, row: number, column: number, text: string) {
        await allure.step(`Check table cell in ${part} , row:${row} column:${column} have text: ${text}`, async () => {
            await expect(this.page.locator(part).locator('tr').nth(row-1)
                .locator(tag).nth(column-1)).toHaveText(text);
        });
    }

    private async checkSubtitleText(text: string) {
        await allure.step("Check subtitle contains: " + text, async () => {
            await expect(this.page.locator('h2')).toContainText(this.subTitleText);
        });
    }

    private async formIsVisible() {
        await allure.step(`Wait for form is visible`, async () => {
            await this.page.locator('form').isVisible();
        });
    }

    private async fillInput(loc: string, text: string) {
        await allure.step(`Fill input ${loc} with text: ${text}`, async () => {
            await this.page.locator(loc).fill(text);
        });
    }

    private async clickButtonByNameAndRow(name: string, row: number) {
        await allure.step(`Click button by name: ${name} at row: ${row}`, async () => {
            await this.page.getByRole('button', { name: name }).nth(row-1).click();
        });
    }

    private async clickButtonByDataQaAndRow(dataQa: string, row: number) {
        await allure.step(`Click button by data-qa: ${dataQa} at row: ${row}`, async () => {
            await this.page.locator(`[data-qa='${dataQa}']`).nth(row-1).click();
        });
    }

    private async clickButtonByText(text: string) {
        await allure.step(`Click button by text: ${text}`, async () => {
            await this.page.getByText(text).click();
        });
    }

}